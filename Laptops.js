
// Below for Select/option-script

let selectBox;

function changeFunc() {
    selectBox = document.getElementById("compChoice");
    let selectedValue = selectBox.options[selectBox.selectedIndex].value;
    //console.log(selectedValue + " was pressed as option.")

    //Handcrafted selectsection. Couldn't create autogenerated as all constructors still need variations. 
   if(selectedValue == 0){
       //Small laptop select section, which shows the features
    document.getElementById("context1").innerHTML = azuz.getSpeaker()
    document.getElementById("context2").innerHTML = azuz.getMonitor()
    document.getElementById("context3").innerHTML = azuz.getDesign()
    console.log("The azuz was entered")

    //Big info section below
    //document.getElementById("image").innerHTML = "<img src=\"assets/appel.jpg\">"
    document.getElementById("image").innerHTML = "<img src='assets/azuz.jpeg'   width='300px' heigth='auto'> "
    document.getElementById("price").innerHTML = "Price: " + azuz.getAzuzPrice()
    }

    if(selectedValue == 1){
        //Small laptop select section, which shows the features
        document.getElementById("context1").innerHTML = ployStation.getDesign()
        document.getElementById("context2").innerHTML = ployStation.getTech()
        document.getElementById("context3").innerHTML = ployStation.getResolution()
        console.log("The ployStation was entered");

    //Big info section below
    //document.getElementById("image").innerHTML = "<img src=\"assets/appel.jpg\">"
    document.getElementById("image").innerHTML = "<img src='assets/ps4.jpg'   width='100%' heigth='100%'> "
    document.getElementById("price").innerHTML = "Price: " + ployStation.getPloyStationPrice()

    }
    if(selectedValue == 2){
        //Small laptop select section, which shows the features
        document.getElementById("context1").innerHTML = lenavah.getSize()
        document.getElementById("context2").innerHTML = lenavah.getTech()
        document.getElementById("context3").innerHTML = lenavah.getMonitor()
        console.log("The lenavah was entered");
    
    //Big info section below
    //document.getElementById("image").innerHTML = "<img src=\"assets/appel.jpg\">"
    document.getElementById("image").innerHTML = "<img src='assets/lenovo.jpeg'   width='100%' heigth='100%'> "
    document.getElementById("price").innerHTML = "Price: " + lenavah.getLenavahPrice()
    }
    if(selectedValue == 3){
        //Small laptop select section, which shows the features
        document.getElementById("context1").innerHTML = dall.getDesign()
        document.getElementById("context2").innerHTML = dall.getTech()
        document.getElementById("context3").innerHTML = dall.getSmooth()
        console.log("The dall was entered");
   
    //Big info section below
    //document.getElementById("image").innerHTML = "<img src=\"assets/appel.jpg\">"
    document.getElementById("image").innerHTML = "<img src='assets/dell.jpeg'   width='100%' heigth='100%'> "
    document.getElementById("price").innerHTML = "Price: " + dall.getDallPrice()
    }
    if(selectedValue == 4){
        //Small laptop select section, which shows the features
        document.getElementById("context1").innerHTML = appel.getSmooth2()
        document.getElementById("context2").innerHTML = appel.getTech2()
        document.getElementById("context3").innerHTML = appel.getDesign()
        console.log("The appel was entered");
    
    //Big info section below
    //document.getElementById("image").innerHTML = "<img src=\"assets/appel.jpg\">"
    document.getElementById("image").innerHTML = "<img src='assets/appel.jpg'   width='100%' heigth='100%'> "
    document.getElementById("price").innerHTML = "Price: " + appel.getAppelPrice()
    }
}


//Declaring a Laptops class. 
class Laptops {

    //Invoked on -< new Laptop()
    constructor(){
        //properties
        this.speaker = "Best in slot speakers";
        this.monitor = "200Hz monitor";
        this.resolution = "Top graphics";
        this.design = "fancy af"
        this.tech = "9/10 tech specs"
        this.tech2 = "2/10 tech specs, fairly useless?"

        this.size = "22\" monitor"
        this.smooth = "Runs real smooth"
        this.smooth2 = "It's laggy boi"
        this.azuzPrice = "1900"
        this.ployStationPrice = "2600"
        this.lenavahPrice = "1220"
        this.dallPrice = "4200"
        this.appelPrice = "5000"
        
    }
    
    //Methods, pretty boring code. Could most probably efficiate this
    getFullLaptop() {
        return `-${this.speaker} \n -${this.monitor} \n -${this.resolution}`;
    }
    getSpeaker() {
        return `- ${this.speaker}`
    }
    getMonitor() {
        return `- ${this.monitor}`
    }
    getResolution() {
        return `- ${this.resolution}`
    }
    getDesign() {
        return `- ${this.design}`
    }
    getTech() {
        return `- ${this.tech}`
    }
    getTech2() {
        return `- ${this.tech2}`
    }
    getSize() {
        return `- ${this.size}`
    }
    getSmooth(){
        return `- ${this.smooth}`
    }
    getSmooth2(){
        return `- ${this.smooth2}`
    }
    getAzuzPrice(){
        return `${this.azuzPrice}`
    }
    getPloyStationPrice(){
        return `${this.ployStationPrice}`
    }
    getLenavahPrice(){
        return `${this.lenavahPrice}`
    }
    getDallPrice(){
        return `${this.dallPrice}`
    }
    getAppelPrice(){
        return `${this.appelPrice}`
    }
}

//Invoke or instatiate the laptop class with new data:
const azuz = new Laptops()
const ployStation = new Laptops()
const lenavah = new Laptops()
const dall = new Laptops()
const appel = new Laptops()

let laptopsCollection = [azuz, ployStation, lenavah, dall, appel]


