
//Initialize some variables globally
let workAccount = 0;
let bankAccount = 0;
let loanMessage = 0;
let totalBalance = 0;

//When work-button is pushed, a work is made and 100 is added to account. 
function workFunction() {

    console.log("Work was made.")
    workAccount += 100;
    document.getElementById("workInput").innerHTML = workAccount + " kr";

}
//Function for adding the workaccount into the bank balance. 
function bankFunction() {

    console.log("Bank balance was updated.")
    bankAccount += workAccount
    document.getElementById("bankBalance").innerHTML = "Bank balance: " + bankAccount + " kr"
    
    //Resets the workAccount to 0
    workAccount = 0;
    document.getElementById("workInput").innerHTML = workAccount + " kr";

    //Updates the total balance. 
    totalBalanceFunction();
}

//Try to get a loan
function getALoanFunction() {
    console.log("A loan was attempted...")
    //Straight into the prompt
    loanMessage = prompt("Please input loan amount!", " kr")

    //Conditions for loan-taking. Also printing to console. 
    if ((loanMessage/2)>bankAccount) {
        console.log("Not approved for the loan")
        window.alert("Not approved for the loan")
    } else if(loanMessage<0) {
        console.log("Too low of a value")
        window.alert("Too low of a value")

    } else {
        console.log("Loan approved!")
        window.alert("Loan approved!")
        document.getElementById("loanBalance").innerHTML = "Loan balance: "+ loanMessage + " kr"

        //(if approved for loan) -> Calling below function to also update the total Balance
        totalBalanceFunction();
    }
}

//I felt a need to seperate the balance. The total balance function is called whenever any of the balances is updated. 
function totalBalanceFunction(){
    console.log("The total balance was calculated!")
    //The unary plus operator to convert them to numbers when calculating the total balance. 
    totalBalance = (+bankAccount + +loanMessage)
    document.getElementById("totalBalance").innerHTML = "Total balance: " + totalBalance + " kr"

}

function purchase() {
    
    //Tried to call the price from the other .js file, counldnt manage..
    //let price = azuz.getAzuzPrice()
    //This just returns a string and parseInt makes in NaN.. Hardcoding the price instead..

    let price = 0;
    //Get value of drop down bar. Depending on choice, set price
    selectBox = document.getElementById("compChoice");
    let selectedValue = selectBox.options[selectBox.selectedIndex].value
        if(selectedValue == 0){
            //Azuz
            price = 1900
        }if(selectedValue == 1){
            //PloyStation
            price = 2600
        }if(selectedValue == 2){
            //Lenavah
            price = 1220
        }if(selectedValue == 3){
            //Dall
            price = 4200
        }if(selectedValue == 4){
            //Appel
            price = 5000
        }
    let temp = 0

    //Checks the price to the totalbalance.
    console.log(typeof price)
    console.log("The comp costs " + price)
        //If totalbalance, the price will go through
    if(totalBalance>price){
        window.alert("The computer was purchased")
        //If loan is greater than price, pay everything with loan
        if(price<=loanMessage){
            loanMessage = loanMessage-price
            console.log("The computer was fully payed by loan.")
            document.getElementById("loanBalance").innerHTML = "Loan balance: "+ loanMessage + " kr"
        }
        //If loan is not big enough, pay with some loan and some cash 
        else if(loanMessage>0 && loanMessage<price) {
            price = price - loanMessage
            bankAccount = bankAccount - price
            console.log("The computer was purchased with some loan.")
            loanMessage = 0
            document.getElementById("loanBalance").innerHTML = "Loan balance: "+ loanMessage + " kr"
            document.getElementById("bankBalance").innerHTML = "Bank balance: " + bankAccount + " kr"
            totalBalanceFunction()
        } 
        //Else pay everything cash 
        else {
            bankAccount = bankAccount - price
            console.log("Fully payed with cash")
            document.getElementById("bankBalance").innerHTML = "Bank balance: " + bankAccount + " kr"
            totalBalanceFunction()
        }
    } else {
        window.alert("Insufficient funds!")
    }
}